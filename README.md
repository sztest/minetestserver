# Warr1024's Minetest Server Docker Image

## Features:

- Lightweight and simple
  - Alpine-based
  - No unnecessary dependencies
  - Simple script wrappers
  - Non-run-in-place build
- Full-featured
  - Support for all official database backends (e.g. LevelDB, Redis, PostgreSQL)
  - Full client version information patch
  - Prometheus support
  - Fast restart loop without restarting container
  - Minetestmapper included
- Simple configuration
  - Mount minetest dir as `/data`, overrides anything built-in
  - Must provide your own base game (minetest_game NOT included)
- Extensible scripting
  - Override `/etc/run-mt-server.sh` in a derivative image or with a bind-mount
- Additional debugging images
  - Add `-gdb` to image tag to include automatic crash catcher with backtrace
    - backtraces will be written to the docker stdout/stderr log
  - Add `-perf` to image tag to include automatic performance profiling
    - requires `--cap-add PERFMON` in docker options
    - requires `kernel.perf_event_paranoid=1` or lower in sysctl
    - profile data will be written to `perf/perfdata-*` in your mounted data volume
