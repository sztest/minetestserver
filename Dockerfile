########################################################################
# BASE TOOLS
# ----------------------------------------------------------------------

FROM alpine:3.20.3 AS base

# ----------------------------------------------------------------------

FROM base AS dlbase

RUN apk add --no-cache curl

# ----------------------------------------------------------------------

FROM base AS gitbase

RUN apk add --no-cache git

########################################################################
# DOWNLOAD SOURCES
# ----------------------------------------------------------------------

FROM dlbase AS dljit

WORKDIR /dl
ARG JITREF
RUN curl -sLf https://github.com/LuaJIT/LuaJIT/archive/${JITREF}.tar.gz | tar xzf -
RUN mv "`ls | grep ^LuaJIT | head -n 1`" luajit

# ----------------------------------------------------------------------

FROM dlbase AS dlmapper

WORKDIR /dl
ARG MAPREF
RUN curl -sLf https://github.com/luanti-org/minetestmapper/archive/${MAPREF}.tar.gz | tar xzf -
RUN mv "`ls | grep ^minetest | head -n 1`" minetestmapper

# ----------------------------------------------------------------------

FROM gitbase AS dlmt

WORKDIR /dl
ARG MTFORK
ARG MTREF
RUN git clone -n https://github.com/${MTFORK}/luanti.git luanti
RUN cd luanti && git checkout -b local ${MTREF}

# ----------------------------------------------------------------------

FROM gitbase AS dlpromcpp

WORKDIR /dl
RUN git clone https://github.com/jupp0r/prometheus-cpp /dl/pcpp
WORKDIR /dl/pcpp
ARG PROMCPPREF
RUN git checkout -b local ${PROMCPPREF}
RUN git submodule update --init --recursive

########################################################################
# BUILD ENVIRONMENT
# ----------------------------------------------------------------------

FROM base AS buildenv

RUN apk add --no-cache \
	bzip2-dev \
	cmake \
	curl-dev \
	doxygen \
	g++ \
	gcc \
	gd-dev \
	git \
	gmp-dev \
	hiredis-dev \
	icu-dev \
	jq \
	leveldb-dev \
	openssl-dev \
	libtool \
	make \
	ncurses-dev \
	python3-dev \
	sqlite-dev \
	zstd-dev \
	postgresql-dev

########################################################################
# BUILDING COMPONENTS
# ----------------------------------------------------------------------

FROM buildenv AS jitbuild

WORKDIR /build
COPY --from=dljit /dl/luajit/ /build/
RUN make -j4
RUN make -j4 install

# ----------------------------------------------------------------------

FROM buildenv AS promcpp

WORKDIR /build
COPY --from=dlpromcpp /dl/pcpp/ /build/
WORKDIR /build/build
RUN cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local -DENABLE_TESTING=0
RUN make -j4
RUN make install

# ----------------------------------------------------------------------

FROM buildenv AS mapbuild

WORKDIR /build
COPY --from=dlmapper /dl/minetestmapper/ /build/
RUN cmake .
RUN make -j4
RUN if [ -f minetestmapper ]; \
	then ln -s minetestmapper luantimapper; \
	else ln -s luantimapper minetestmapper; \
	fi
RUN mkdir -p /built && cp -P minetestmapper luantimapper /built/

# ----------------------------------------------------------------------

FROM base AS mtpatch

RUN apk add --no-cache perl

COPY --from=dlmt /dl/ /dl/

COPY ./patches/ /patches/
WORKDIR /dl/luanti
RUN find /patches -type f | sort | xargs -rt chmod +x
RUN find /patches -type f | sort | xargs -rtn 1 sh -c

# ----------------------------------------------------------------------

FROM buildenv AS mtbuild

WORKDIR /build
COPY --from=mtpatch /dl/luanti/ /build/
COPY --from=promcpp /usr/local/lib/ /built/usr/local/lib/
COPY --from=promcpp /usr/local/include/ /usr/local/include/
COPY --from=jitbuild /usr/local/lib/ /usr/local/lib/
COPY --from=jitbuild /usr/local/include/ /usr/local/include/

RUN cmake \
	-DBUILD_CLIENT=0 \
	-DBUILD_SERVER=1 \
	-DRUN_IN_PLACE=0 \
	-DUSE_CURL=1 \
	-DUSE_LUAJIT=1 \
	-DUSE_LEVELDB=1 \
	-DUSE_REDIS=1 \
	-DUSE_POSTGRESQL=1 \
	-DENABLE_GETTEXT=0 \
	-DENABLE_SOUND=0 \
	-DENABLE_SYSTEM_GMP=1 \
	-DENABLE_CURSES=0 \
	-DENABLE_PROMETHEUS=1 \
	.

RUN make -j4
RUN make -j4 install

RUN for x in lib bin share; do mkdir -p /built/usr/local/$x; done
COPY --from=jitbuild /usr/local/lib/ /built/usr/local/lib/
RUN if [ -f /usr/local/bin/minetestserver ];\
	then cp /usr/local/bin/minetestserver /built/usr/local/bin; \
	else cp /usr/local/bin/luantiserver /built/usr/local/bin; \
	fi
RUN cd /built/usr/local/bin && ( [ -f minetestserver ] && \
	ln -s minetestserver luantiserver || \
	ln -s luantiserver minetestserver )
RUN if [ -d /usr/local/share/minetest ]; \
	then cp -R /usr/local/share/minetest /built/usr/local/share; \
	else cp -R /usr/local/share/luanti /built/usr/local/share; \
	fi
RUN cd /built/usr/local/share && ( [ -d minetest ] && \
	ln -s minetest luanti ||\
	ln -s luanti minetest )

########################################################################
# COMBINE BUILDS
# ----------------------------------------------------------------------

FROM base AS assemble

RUN apk add --no-cache lua5.1-socket

RUN mkdir -p /assemble/usr/local/lib/lua/5.1 \
	&& mv -f /usr/lib/lua/5.1/* /assemble/usr/local/lib/lua/5.1/

COPY --from=jitbuild /usr/local/lib/ /assemble/usr/local/lib/
COPY --from=mtbuild /built/ /assemble/
COPY --from=mapbuild /built/ /assemble/usr/local/bin/

########################################################################
# RUNTIME ENVS
# ----------------------------------------------------------------------

FROM base AS runenv

ARG UID=1000
RUN adduser -h /home/app -D -u ${UID} app

RUN mkdir -p /data \
	&& rm -rf /home/app/.minetest \
	&& ln -sf /data /home/app/.minetest \
	&& rm -rf /home/app/.luanti \
	&& ln -sf /data /home/app/.luanti \
	&& chown -R app:app /data /home/app \
	&& chmod 01777 /data

RUN apk add --no-cache \
	tzdata \
	curl \
	gmp \
	hiredis \
	leveldb \
	libgcc \
	libintl \
	libpq \
	libstdc++ \
	sqlite-libs \
	zstd-libs \
	gd

ENV PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
ENV HOME=/home/app

VOLUME [ "/data" ]
ENTRYPOINT [ "/bin/sh", "/etc/entry.sh" ]

# N.B. do NOT copy MT builds or wrappers into here yet, as those may
# change frequently, so they should not be beneath the layers where
# very large things like gdb are installed.

# ----------------------------------------------------------------------

FROM runenv AS rungdb

RUN apk add --no-cache gdb

# ----------------------------------------------------------------------

FROM runenv AS runperf

RUN apk add --no-cache perf

########################################################################
# FINAL IMAGES
# ----------------------------------------------------------------------

FROM runenv AS finalbasic

USER app

COPY --from=assemble /assemble/ /
COPY ./app-basic/ /

# ----------------------------------------------------------------------

FROM runenv AS finalgdb

RUN apk add --no-cache gdb

USER app

COPY --from=assemble /assemble/ /
COPY ./app-basic/ /
COPY ./app-gdb/ /

# ----------------------------------------------------------------------

FROM runenv AS finalperf

RUN apk add --no-cache perf

USER app

COPY --from=assemble /assemble/ /
COPY ./app-basic/ /
COPY ./app-perf/ /
