#!/bin/sh
# Docker entry point and main loop

# SIGHUP to quickly restart server
trap 'pkill -TERM minetestserver; wait' HUP

# Other termination signals to shut down container
trap 'STOP=1; pkill -TERM minetestserver; wait' TERM INT QUIT

unset STOP
while :; do
	# Child process must be background
	# https://stackoverflow.com/a/75201306
	/bin/sh /etc/run-mt-server.sh "$@" &
	wait

	echo "\$? = $?"

	[ -n "$STOP" ] && break
	sleep 1
done
