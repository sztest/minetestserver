#!/usr/bin/env perl
use strict;
use warnings;

my @lines;

my $skipndef;
my $skipend;

open(my $fh, "<", "src/script/lua_api/l_server.cpp") or die($!);
while(<$fh>) {
	if(m#ModApiServer::l_get_player_information#) {
		warn("get_player_information found\n");
		$skipndef = 1;
		$skipend = 1;
	}
	if($skipndef and m|^\s*#ifndef\s+NDEBUG\b|) {
		warn("SKIP: $_");
		$skipndef = 0;
	} elsif($skipend and m|\s*#endif\b|) {
		warn("SKIP: $_");
		$skipend = 0;
	} else {
		push @lines, $_;
	}
}
close($fh);

open($fh, ">", "src/script/lua_api/l_server.cpp") or die($!);
map { print $fh $_ } @lines;
close($fh);
