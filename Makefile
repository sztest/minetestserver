MTFORK=luanti-org
MTVER=5.10.0
JITVER=97813fb924edf822455f91a5fbbdfdb349e5984f
PROMCPPVER=052fd18c8e27e4f9fb3e183382ccfba4257f0305
MAPPERVER=1c16c40ccca7039e7e03f9a81909c1d3e5fd11c1
NAME=minetestserver
LTAG=warr1024/${NAME}:latest
VTAG=warr1024/${NAME}:${MTVER}
ARCH=`uname -m | grep -v x86_64 | sed 's/^/-/;s/-$$//'`
ALTAG=${LTAG}${ARCH}
AVTAG=${VTAG}${ARCH}

sh: build
	docker run -it --rm --name="${NAME}" \
		--entrypoint '' \
		-u 0 \
		"${AVTAG}" /bin/sh

pushall: vpushv mpushv mpushl

buildall: build mbuild

vpushl: build
	docker tag "${AVTAG}" "${ALTAG}"
	docker push "${ALTAG}"

vpushv: build
	docker push "${AVTAG}"
	docker push "${AVTAG}-gdb"
	docker push "${AVTAG}-perf"

build:
	find . -type d -name bin | xargs -I X find X -type f | xargs chmod 755
	env DOCKER_BUILDKIT=1 docker build --progress=plain \
		--build-arg MTFORK="${MTFORK}" \
		--build-arg MTREF="${MTVER}" \
		--build-arg JITREF="${JITVER}" \
		--build-arg PROMCPPREF="${PROMCPPVER}" \
		--build-arg MAPREF="${MAPPERVER}" \
		-t "${AVTAG}" \
		--target finalbasic \
		-f Dockerfile \
		.
	env DOCKER_BUILDKIT=1 docker build --progress=plain \
		--build-arg MTFORK="${MTFORK}" \
		--build-arg MTREF="${MTVER}" \
		--build-arg JITREF="${JITVER}" \
		--build-arg PROMCPPREF="${PROMCPPVER}" \
		--build-arg MAPREF="${MAPPERVER}" \
		-t "${AVTAG}-gdb" \
		--target finalgdb \
		-f Dockerfile \
		.
	env DOCKER_BUILDKIT=1 docker build --progress=plain \
		--build-arg MTFORK="${MTFORK}" \
		--build-arg MTREF="${MTVER}" \
		--build-arg JITREF="${JITVER}" \
		--build-arg PROMCPPREF="${PROMCPPVER}" \
		--build-arg MAPREF="${MAPPERVER}" \
		-t "${AVTAG}-perf" \
		--target finalperf \
		-f Dockerfile \
		.

mpushl: mbuild
	env MTVER=master make -e vpushl

mpushv: mbuild
	env MTVER=master make -e vpushv

mbuild:
	env MTVER=master make -e build
